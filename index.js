$(function tooltip() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({interval:2000});
  })
    $('#contacto').on('show.bs.modal', function (e) {
      console.log('se esta mostrando el modal')

      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-success');
      $('#contactoBtn').prop('disabled', true);


    })
    $('#contacto').on('shown.bs.modal', function (e) {
      console.log('se mostro el modal')
    })
    $('#contacto').on('hide.bs.modal', function (e) {
      console.log('se esta ocultando el modal')
    })
    $('#contacto').on('hidden.bs.modal', function (e) {
      console.log('se oculto el modal')
      $('#contactoBtn').prop('disabled', false);

    })